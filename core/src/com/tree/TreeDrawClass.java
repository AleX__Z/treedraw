package com.tree;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.tree.lang.src.Tree;
import com.tree.lang.src.Vector;

import java.util.Arrays;


public class TreeDrawClass extends ApplicationAdapter {
	private static final float RADIUS_CYLINDER = 0.01f;
	private static final int DIVISION = 4;
	private static final int COUNT_POINT_TREE = 25;

	Array<ModelInstance> modelTree = new Array<ModelInstance>();
	ModelBatch batch;
	Texture img;
	TreeCamera cam;
	private CameraInputController controller;
	ModelBuilder builder;
	Tree tree;
	Environment environment = new Environment();
	private boolean loading = true;


	@Override
	public void create() {
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
		batch = new ModelBatch();
		builder = new ModelBuilder();
		tree = Tree.createTree(COUNT_POINT_TREE);
		cam = new TreeCamera();
		controller = new CameraInputController(cam);
		Gdx.input.setInputProcessor(controller);
	}

	@Override
	public void render() {
		if (loading)
			doneLoading();

		controller.update();
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		//Gdx.gl.glClearColor(135/255f, 206/255f, 235/255f, 1);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		batch.begin(cam);
		batch.render(modelTree, environment);
		batch.end();
	}

	private void doneLoading() {
		drawTree(tree, tree);
		drawAxis();
		loading = false;
	}

	private void drawTree(Tree tree, Tree parentNode) {
		for (Tree node : tree.childs) {
			drawTree(node, tree);
		}

		Model model = builder.createCylinder(
				RADIUS_CYLINDER,
				getHeight(tree, parentNode),
				RADIUS_CYLINDER,
				DIVISION,
				new Material(ColorAttribute.createDiffuse(Color.GREEN)),
				VertexAttributes.Usage.Position |
						VertexAttributes.Usage.Normal |
						VertexAttributes.Usage.TextureCoordinates

		);

		ModelInstance shipInstance = new ModelInstance(model, getCoordinateCylinder(tree, parentNode));
		modelTree.add(shipInstance);
	}

	private void drawAxis() {
		int lengthArrow = 1;
		int[] flags = new int[3];
		for (int i = 0; i < flags.length; i++) {
			Arrays.fill(flags, 0);
			flags[i] = 1;
			Model model = builder.createArrow(
					new Vector3(0, 0, 0),
					new Vector3(flags[0] * lengthArrow, flags[1] * lengthArrow, flags[2] * lengthArrow),
					new Material(ColorAttribute.createDiffuse(flags[0], flags[1], flags[2], 1)),
					VertexAttributes.Usage.Position |
							VertexAttributes.Usage.Normal |
							VertexAttributes.Usage.TextureCoordinates
			);

			ModelInstance shipInstance = new ModelInstance(model, 0, 0, 0);
			modelTree.add(shipInstance);
		}
	}

	private float getHeight(Tree first, Tree second) {
		return (float) first.getCurrentVertex().dist(second.getCurrentVertex());
	}

	private Vector3 getCoordinateCylinder(Tree first, Tree second) {
		Vector[] v = new Vector[]{first.getCurrentVertex(), second.getCurrentVertex()};
		return new Vector3(
				(float) (v[0].x + v[1].x) / 2f,
				(float) (v[0].y + v[1].y) / 2f,
				(float) (v[0].z + v[1].z) / 2f);
	}
}
