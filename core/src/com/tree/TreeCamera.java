package com.tree;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Alex on 24.03.2015.
 */
public class TreeCamera extends PerspectiveCamera {
	public static final Vector3 LOOK_AT = new Vector3(0, 0, 0);
	public Vector3 START_POSITION = new Vector3(0, 3, 3);

	public TreeCamera() {
		super(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		position.set(START_POSITION);
		lookAt(LOOK_AT);
		near = 0.1f;
		far = 300f;
		update();
	}
}
